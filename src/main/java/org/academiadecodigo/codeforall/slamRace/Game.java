
package org.academiadecodigo.codeforall.slamRace;

import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGrid;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.codeforall.slamRace.objects.Decor;
import org.academiadecodigo.codeforall.slamRace.objects.GameObject;
import org.academiadecodigo.codeforall.slamRace.player.Car;
import org.academiadecodigo.codeforall.slamRace.objects.GameObjectFactory;
import org.academiadecodigo.codeforall.slamRace.objects.Obstacle;
import org.academiadecodigo.codeforall.slamRace.player.Direction;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Game {

    private GameObject[] obstacles;
    private Decor[] decors;
    private Car player;
    private Picture background;
    private int delay;
    private int countDecor = 0;

    public boolean gameRunning;
    private boolean gameOver;

    MyVerySpecialKeyHandler keyHandler = new MyVerySpecialKeyHandler(this);

    public Game(int numberOfObjects) {
        delay = Config.DELAY;
        obstacles = new GameObject[numberOfObjects+1];
        decors = new Decor[5];
        player = new Car(new SimpleGraphicGridPosition(Config.CAR_X_POSITION, GameLane.CENTER.getYPosition()), PicturesFactory.car());
        obstacles[numberOfObjects]=player;
        gameRunning = false;
    }

    public void initGame() {

        SimpleGraphicGrid grid = new SimpleGraphicGrid(Config.BACKGROUND_WIDTH, Config.BACKGROUND_HEIGHT);
        background = PicturesFactory.startBackground();
        background.draw();

        for (int i = 0; i < obstacles.length-1; i++) {
            obstacles[i] = GameObjectFactory.createObstacle();
        }

        for (int i = 0; i < decors.length; i++) {
            //   Picture rock = PicturesFactory.rock();
            decors[i] = GameObjectFactory.createDecor((i * 100) + Config.PADDING);
        }

        grid.init();
        keyHandler.init();

        while (true) {
            System.out.println("happening");
            if (gameRunning) {
                System.out.println("It happen!");
                try {
                    start();
                } catch (InterruptedException e) {
                    System.out.println("Some shit happen");
                }


            }
        }
    }


    private void startGame() {
        background.delete();
        background = PicturesFactory.gameBackground();
        background.draw();

    }

    public void start() throws InterruptedException {

        gameOver = false;
        gameRunning = true;

        startGame();
        player.initObject();

        int counter = 0;
        while (!player.isCrashed()) {
            counter++;


            moveGameObjects();
            Thread.sleep(delay);

        }

        gameRunning = false;
        gameOver = true;
        gameOver();


    }

    private GameObject getAnObstacle() {
        boolean canMakeAnObstacle = true;


        for (GameObject obstacle : obstacles) {
            if (obstacle.getX() > 1000) {

                canMakeAnObstacle = false;
            }

        }
        if (canMakeAnObstacle) {
            return reuseAssets();
        }
        return null;
    }

    /*
    private Decor reuseDecors(){

        int random = (int) (Math.random() * decors.length);

        if(decors[random].getY() <)



        return null;
    }

     */


    private GameObject reuseAssets() {

        int random = (int) (Math.random() * obstacles.length);

        if ((obstacles[random].getX() > 0 && obstacles[random].getX() < 1400)||random==obstacles.length-1) {
            return reuseAssets();
        }
        obstacles[random].restartMove();
        return obstacles[random];
    }


    public void gameOver() {
        System.out.println("Game Over");
        background.delete();
        background = PicturesFactory.gameOverBackground();
        background.draw();

    }

    private boolean checkCollision() {

        for (GameObject obstacle : obstacles) {

            if(obstacle==player){
                continue;
            }
            if (obstacle.getX() > player.getX() + player.getPicture().getWidth()) {
                continue;
            }

            int playerY = player.getY();
            int obstacleY = obstacle.getY();


            if (Config.yLogicalPosition(playerY) == Config.yLogicalPosition(obstacleY)) {
                player.crash();
                System.out.println("new Image in Game check Colision");
                return true;
            }

        }


        // se o objecto posicao menor que a posicao do carro
        // translate (dx, randomY)
        // x0 = PADDING
        //
        return false;

    }

    private void moveGameObjects() {

        //useDecor();

        for (int i = 0; i < obstacles.length; i++) {
            obstacles[i].move();
            decors[i].move();
        }

        checkCollision();
    }


    private void useDecor() {

        if (countDecor < decors.length && decors[countDecor] == null) {
//            decors[countDecor] = GameObjectFactory.createDecor();
        }

        if (countDecor >= decors.length) {
            countDecor = 0;

        }

        decors[countDecor].move();
        countDecor++;

    }

    public Car getPlayer() {
        return player;
    }

}
