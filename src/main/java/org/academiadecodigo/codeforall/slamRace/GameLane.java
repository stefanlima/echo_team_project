package org.academiadecodigo.codeforall.slamRace;

public enum GameLane {

    UP(283),
    CENTER(433),
    DOWN(583),
    ROCK(140),
    TREES(7);

    private final int yPosition;

    GameLane(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

}
