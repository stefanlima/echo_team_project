package org.academiadecodigo.codeforall.slamRace.grid;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class SimpleGraphicGrid {

    private int x;
    private int y;

    private Rectangle canvas;

    public SimpleGraphicGrid(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void init(){
        canvas = new Rectangle(Config.PADDING,Config.PADDING, Config.BACKGROUND_WIDTH,Config.BACKGROUND_HEIGHT);
        canvas.draw();
    }

}
