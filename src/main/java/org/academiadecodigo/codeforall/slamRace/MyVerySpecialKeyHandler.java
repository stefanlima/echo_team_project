package org.academiadecodigo.codeforall.slamRace;

import org.academiadecodigo.codeforall.slamRace.player.Direction;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class MyVerySpecialKeyHandler implements KeyboardHandler {
    private Keyboard keyboard;
    private Game game;

    public MyVerySpecialKeyHandler(Game thisGame){
        game=thisGame;
        keyboard = new Keyboard(this);
    }



    public void init(){

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent releaseUp = new KeyboardEvent();
        releaseUp.setKey(KeyboardEvent.KEY_UP);
        releaseUp.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent releaseDown = new KeyboardEvent();
        releaseDown.setKey(KeyboardEvent.KEY_DOWN);
        releaseDown.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);

        KeyboardEvent start = new KeyboardEvent();
        releaseUp.setKey(KeyboardEvent.KEY_SPACE);
        releaseUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent space = new KeyboardEvent();
        releaseDown.setKey(KeyboardEvent.KEY_SPACE);
        releaseDown.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);



        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(start);

        keyboard.addEventListener(releaseUp);
        keyboard.addEventListener(releaseDown);
        keyboard.addEventListener(space);


    }



    @Override
    public void keyPressed(KeyboardEvent event) {

        switch (event.getKey()) {
            case KeyboardEvent.KEY_UP:
                game.getPlayer().direction = Direction.UP;
                break;

            case KeyboardEvent.KEY_DOWN:
                game.getPlayer().direction = Direction.DOWN;
                break;
            case KeyboardEvent.KEY_SPACE:
                game.gameRunning=true;
                break;
        }

    }

    @Override
    public void keyReleased(KeyboardEvent event) {
        game.getPlayer().direction = Direction.DEFAULT;
    }


}
