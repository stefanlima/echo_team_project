package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Barrel extends Obstacle {
    public Barrel(SimpleGraphicGridPosition position, String source) {
        super(position, new Picture((double)position.getX(), (double)position.getY(), source));
    }
}