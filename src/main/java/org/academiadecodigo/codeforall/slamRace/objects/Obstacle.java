package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Obstacle extends GameObject {

    public Obstacle(SimpleGraphicGridPosition position, Picture picture){
        super(position, picture);
    }

    @Override
    public int randomY() {

        int randomY = Config.randomYBarrel();
        int newY = 0;

        if (randomY > getPicture().getY()) {
            newY = randomY - getPicture().getY();
        }

        if (randomY < getPicture().getY()) {
            newY = randomY - getPicture().getY();
        }

        return newY;

    }

}
