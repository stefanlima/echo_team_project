package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Tree extends Decor {

    public Tree(int initialX){
        super();
        int index = (int) (Math.random() * (TreeType.values().length));
        TreeType treeType = TreeType.values()[index];
        int treeY = 13;
        int treeWidth = 220;
        setPicture(new Picture(initialX, treeY, treeType.source));
        setPosition(new SimpleGraphicGridPosition(initialX, getPicture().getY()));
    }

    private enum TreeType {

        TREE1("/image/tree1.png"),
        TREE2("/image/tree2.png");

        private String source;

        TreeType(String source){
            this.source = source;
        }

    }

}
