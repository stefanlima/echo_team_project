package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.PicturesFactory;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameObjectFactory {



    public static Decor createDecor(int xPosition) {
        return Math.random() > 0.5 ? new Tree(xPosition) : new Rock(xPosition);

    }


    // Correct Random x,y initial

    public static Obstacle createObstacle() {
        // BarrelType type = BarrelType.values()[(int)(Math.random() * BarrelType.values().length)];
        //if(type==BarrelType.ORANGE){source = "src/resources/barrelOrange.png";}
        //else if(type==BarrelType.RED){source = "src/resources/barrelRed.png";}
        //else if(type==BarrelType.YELLOW) {source = "src/resources/barrelYellow.png";}
        // set X to picture size
        Picture picture = PicturesFactory.barrel();
        return new Obstacle(new SimpleGraphicGridPosition(picture.getX(), picture.getY()), picture);
    }

}
