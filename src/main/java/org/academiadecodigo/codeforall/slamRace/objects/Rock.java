package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Rock extends Decor {

    public Rock(int initialX){
        super();
        int rockY = 75;
        int rockWidth = 220;
        String source = "/image/rock.png";
        setPicture(new Picture(initialX, rockY, source));
        setPosition(new SimpleGraphicGridPosition(initialX, getPicture().getY()));
    }

}
