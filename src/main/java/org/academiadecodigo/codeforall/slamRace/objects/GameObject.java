package org.academiadecodigo.codeforall.slamRace.objects;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public abstract class GameObject {

    private SimpleGraphicGridPosition position;
    private Picture picture;
    private int speed;

    public GameObject() {
        speed = Config.SPEED * Config.CELL_SIZE;
    }

    public GameObject(SimpleGraphicGridPosition position, Picture picture) {
        this.position = position;
        this.picture = picture;
        speed = Config.SPEED * Config.CELL_SIZE;
    }

    public void initObject() {
        this.picture.draw();
    }

    public void move() {
        picture.delete();
        if (picture.getX() <= Config.PADDING+Config.CAR_X_POSITION) {

            picture.translate(Config.BACKGROUND_WIDTH+Config.PADDING-Config.TREE_X_DIFFERENCE+100, randomY());
            position.setX(picture.getX());
            position.setY(picture.getY());
            return;
        }
        picture.translate(-speed, 0);
        position.setX(picture.getX());
        picture.draw();
    }

    public void restartMove() {

        picture.translate(Config.RIGHT_BORDER - (Config.PADDING + picture.getWidth()), this.randomY());
        System.out.println(position.getX()+"  "+picture.getX());
        position.setX(picture.getX());
        position.setY(picture.getY());
        move();
    }

    public int randomY() {
        return 0;
    }

    public Picture getPicture() {
        return picture;
    }

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    protected void setPosition(SimpleGraphicGridPosition position) {
        this.position = position;
    }

    protected void setPicture(Picture picture) {
        this.picture = picture;
    }

}
