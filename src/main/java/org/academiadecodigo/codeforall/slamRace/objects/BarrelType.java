package org.academiadecodigo.codeforall.slamRace.objects;

public enum BarrelType {
    YELLOW(10, "/image/barrelYellow.png"),
    ORANGE(30, "/image/barrelOrange.png"),
    RED(50, "/image/barrelRed.png");

    private int damage;
    private String source;

    private BarrelType(int damage, String source) {
        this.damage = damage;
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}
