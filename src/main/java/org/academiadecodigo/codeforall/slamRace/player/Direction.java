package org.academiadecodigo.codeforall.slamRace.player;

public enum Direction {
    UP,
    DOWN,
    DEFAULT
}
