package org.academiadecodigo.codeforall.slamRace.player;

import org.academiadecodigo.codeforall.slamRace.Config;
import org.academiadecodigo.codeforall.slamRace.GameLane;
import org.academiadecodigo.codeforall.slamRace.grid.SimpleGraphicGridPosition;
import org.academiadecodigo.codeforall.slamRace.objects.GameObject;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Car extends GameObject  {

    public Direction direction;
    private SimpleGraphicGridPosition position;
    private boolean crashed;

    public Car(SimpleGraphicGridPosition position, Picture picture) {
        super(position,  picture);
        this.position = position;
        direction = Direction.DEFAULT;
        crashed = false;
    }





    public void crash() {
        System.out.println("You crashed!");
        crashed = true;
    }

    public boolean isCrashed() {
        return crashed;
    }


    @Override
    public void move() {

        getPicture().delete();

        switch (direction){

            case DEFAULT:
                break;
            case DOWN:
                if(position.getY() == GameLane.DOWN.getYPosition()){
                    break;
                }
                getPicture().translate(0, Config.LANE_HEIGHT);
                position.setY(getPicture().getY());
                break;

            case UP:
                if(position.getY() == GameLane.UP.getYPosition()){
                    break;
                }
                getPicture().translate(0, -Config.LANE_HEIGHT);
                position.setY(getPicture().getY());
                break;
        }

        getPicture().draw();

    }

}
