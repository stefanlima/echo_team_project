
package org.academiadecodigo.codeforall.slamRace;

public final class Config {

    public static final int PADDING = 10;
    public static final int CELL_SIZE = 50;
    public static final int BACKGROUND_WIDTH = 1400;
    public static final int BACKGROUND_HEIGHT = 800;
    public static final int LANE_HEIGHT = 150;
    public static final int RIGHT_BORDER = BACKGROUND_WIDTH - PADDING;
    public static final int SPEED = 7;
    public static final int CAR_X_POSITION = 50 + PADDING;
    public static final int TREE_Y_POSITION = 13;
    public static final int ROCK_Y_POSITION = 230;
    public static final int DELAY = 300;
    public static final int BARREL_Y_DIFFERENCE = 30;
    public static final int BARREL_X_DIFFERENCE = 30;
    public static final int TREE_X_DIFFERENCE = 80;

    public static int randomYBarrel() {

        int yPosition = (int) (Math.random() * 3);

        switch (yPosition) {
            case 0:
                yPosition = GameLane.UP.getYPosition() + Config.BARREL_Y_DIFFERENCE;
                break;
            case 1:
                yPosition = GameLane.CENTER.getYPosition() + Config.BARREL_Y_DIFFERENCE;
                break;
            case 2:
                yPosition = GameLane.DOWN.getYPosition() + Config.BARREL_Y_DIFFERENCE;
                break;
        }

        return yPosition;

    }


    public static int randomYDecor() {

        int decorPosition = (int) (Math.random() * 2);

        switch (decorPosition){

            case 0:
                decorPosition = GameLane.ROCK.getYPosition() + Config.ROCK_Y_POSITION;
                break;
            case 1:
                decorPosition = GameLane.TREES.getYPosition() + Config.TREE_Y_POSITION;
                break;
        }
        return decorPosition;
    }

    public static int yLogicalPosition(int yPosition) {

        if (yPosition >= 253 && yPosition < 400) {
            return 0;
        }

        if (yPosition >= 400 && yPosition < 554) {
            return 1;
        }

        return 2;

    }

}
