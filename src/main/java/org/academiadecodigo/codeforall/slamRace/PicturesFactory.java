
package org.academiadecodigo.codeforall.slamRace;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class PicturesFactory {

    public static Picture gameBackground() {
        return new Picture(Config.PADDING, Config.PADDING, "/image/background.png");
    }
    public static Picture startBackground() {
        return new Picture(Config.PADDING, Config.PADDING, "/image/startgame.png");
    }
    public static Picture gameOverBackground() {
        return new Picture(Config.PADDING, Config.PADDING, "/image/gameover.png");
    }

    public static Picture car() {
        return new Picture(Config.CAR_X_POSITION, GameLane.CENTER.getYPosition(), "/image/car.png");
    }

    public static Picture barrel() {
        /*
        int randomBarrel = (int) (Math.random() * BarrelType.values().length);
        String source = BarrelType.values()[randomBarrel].getSource();
        */
        return new Picture(Config.RIGHT_BORDER -150, Config.randomYBarrel(),"/image/barrelYellow.png");
    }

    public static Picture gas() {
        return null;
    }

    public static Picture tool() {
        return null;
    }

    public static Picture tree() {

            return new Picture(Config.RIGHT_BORDER - 150, Config.randomYDecor(), "/image/tree1.png");

    }


    public static Picture rock() {
        return new Picture(Config.RIGHT_BORDER - 150, Config.randomYDecor(), "/image/rock.png");
    }

}
